import csv
import numpy as np
import subprocess

from argparse import ArgumentParser
from flask import Flask, jsonify, request
from flask_cors import CORS

label_names = ['graph', 'dp', 'geo']

api = Flask(__name__)
CORS(api)


@api.route('/api/v1/predict', methods=['POST'])
def predict():
    args = get_options()
    num_labels = args.num_labels
    data_dir = args.data_dir
    idx = args.checkpoint_index

    data = request.json
    text = data['text']

    with open(data_dir + "/test.tsv", "w") as f:
        txt = '\t'.join([text, "0"])
        f.write(txt + '\n')

    cmd = "env NUM_LABELS={0}" \
          " python run_classifier.py" \
          " --task_name=my" \
          " --do_predict=true" \
          " --data_dir={1}" \
          " --vocab_file={1}/vocab.txt" \
          " --bert_config_file={1}/bert_config.json" \
          " --init_checkpoint={1}/model.ckpt-{2}" \
          " --max_seq_length=128" \
          " --train_batch_size=32" \
          " --learning_rate=2e-5" \
          " --num_train_epochs=3.0" \
          " --output_dir={1}".format(num_labels, data_dir, idx)

    print(cmd)
    subprocess.run(cmd, shell=True)

    lines = []
    with open(data_dir + "/test_results.tsv") as f:
        reader = csv.reader(f, delimiter='\t')
        for line in reader:
            lines.append(line)

    labels = []
    for line in lines:
        labels.append(np.argmax(line))

    idx = int(labels[0])

    result = {
        "label": label_names[idx],
    }

    return jsonify(result)


@api.route('/api/v1/test', methods=['POST'])
def test():
    res = request.json

    ret = {'label': res['text']}

    return jsonify(ret)


def get_options():
    parser = ArgumentParser(description='API server')
    parser.add_argument('-d', '--data_dir')
    parser.add_argument('-n', '--num_labels')
    parser.add_argument('-c', '--checkpoint_index')

    return parser.parse_args()


if __name__ == "__main__":
    api.run()
