import csv
import os
import numpy as np
from argparse import ArgumentParser


def get_options():
    parser = ArgumentParser(description='Compute accuracy of test results')
    parser.add_argument('-d', '--data_dir')
    parser.add_argument('-r', '--result_dir')

    return parser.parse_args()


def read_tsv(path):
    lines = []
    with open(path) as f:
        reader = csv.reader(f, delimiter="\t")
        for line in reader:
            lines.append(line)

    return lines


def get_labels(lines, header=False):
    labels = []
    for i, line in enumerate(lines):
        if i == 0 and header:
            continue
        labels.append(int(line[1]))

    return labels


def calc_labels(lines):
    labels = []
    for line in lines:
        labels.append(np.argmax(line))

    return labels


def compute_accuracy(actual, predict):
    match_cnt = 0
    for i in range(len(actual)):
        if actual[i] == predict[i]:
            match_cnt += 1

    print("Matched: ", match_cnt)
    print("Failed: ", len(actual) - match_cnt)
    print("Accuracy: ", match_cnt / len(actual))


if __name__ == '__main__':
    args = get_options()
    data_dir = args.data_dir
    result_dir = args.result_dir

    actual_labels = get_labels(
        read_tsv(os.path.join(data_dir, "test.tsv")))
    predict_labels = calc_labels(
        read_tsv(os.path.join(result_dir, "test_results.tsv")))

    compute_accuracy(actual_labels, predict_labels)
