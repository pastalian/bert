BERT_BASE_DIR="$HOME/test"
DATA_DIR="$HOME/test"
OUTPUT_DIR="$HOME/test"

NUM_LABELS=3 python run_classifier.py \
  --task_name=my \
  --do_predict=true \
  --data_dir="$DATA_DIR" \
  --vocab_file="$BERT_BASE_DIR"/vocab.txt \
  --bert_config_file="$BERT_BASE_DIR"/bert_config.json \
  --init_checkpoint="$OUTPUT_DIR"/model.ckpt-12 \
  --max_seq_length=128 \
  --train_batch_size=32 \
  --learning_rate=2e-5 \
  --num_train_epochs=3.0 \
  --output_dir="$OUTPUT_DIR"

python compute_accuracy.py \
  --data "$DATA_DIR" \
  --result "$OUTPUT_DIR"
